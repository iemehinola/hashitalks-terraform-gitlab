module "s3" {
  source = "../../modules/s3"

  bucket = "${local.application}-${terraform.workspace}-qwerty"
  tags   = var.tags
}